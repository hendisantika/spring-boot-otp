package com.hendisantika.springboototp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"com.hendisantika"})
@EnableJpaRepositories("com.hendisantika.springboototp.repo")
@EntityScan("com.hendisantika.springboototp.model")
public class SpringBootOtpApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootOtpApplication.class, args);
    }
}
