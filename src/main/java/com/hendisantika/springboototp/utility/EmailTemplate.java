package com.hendisantika.springboototp.utility;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-otp
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/12/17
 * Time: 15.42
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Data
public class EmailTemplate {
    private String templateId;

    private String template;

    private Map<String, String> replacementParams;

    public EmailTemplate(String templateId) {
        this.templateId = templateId;
        try {
            this.template = loadTemplate(templateId);
        } catch (Exception e) {
            this.template = "Empty";
        }
    }

    private String loadTemplate(String templateId) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(templateId).getFile());
        String content = "Empty";
        try {
            content = new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            throw new Exception("Could not read template with ID = " + templateId);
        }
        return content;
    }

    public String getTemplate(Map<String, String> replacements) {
        String cTemplate = this.template;

        log.info("getTemplate --> {}", replacements);

        log.info("=========== Cara Pertama ===========");
        //Replace the String
        for (Map.Entry<String, String> entry : replacements.entrySet()) {
            this.template = cTemplate.replace("{{" + entry.getKey() + "}}", entry.getValue());
            log.info("{{" + entry.getKey() + "}}", entry.getValue());
            log.info("cTemplate --> {}", cTemplate);
        }

//        log.info("=========== Cara Kedua ===========");
//        for (Map.Entry<String, String> entry : replacements.entrySet()) {
//            params = params.replace("{{" + entry.getKey() + "}}", entry.getValue());
//            log.info("{{" + entry.getKey() + "}}", entry.getValue());
//            log.info( "Value {} " + entry.getKey() + ":" + entry.getValue());
//        }
        cTemplate = convertWithStream(replacements);
        log.info("convertWithStream(replacements) --> {}", cTemplate);

        return cTemplate;
    }

    public String convertWithStream(Map<String, String> map) {
        String mapAsString = map.keySet().stream()
                .map(key -> key + "=" + map.get(key))
                .collect(Collectors.joining(", ", "{{", "}}"));
        return mapAsString;
    }
}
