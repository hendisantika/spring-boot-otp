package com.hendisantika.springboototp.repo;

import com.hendisantika.springboototp.model.Book;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-otp
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/12/17
 * Time: 15.36
 * To change this template use File | Settings | File Templates.
 */
public interface BookRepository extends CrudRepository<Book, Long> {

    List<Book> findByTitle(String title);
}
