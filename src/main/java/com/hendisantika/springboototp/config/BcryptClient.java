package com.hendisantika.springboototp.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-otp
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/12/17
 * Time: 15.43
 * To change this template use File | Settings | File Templates.
 */
public class BcryptClient {
    private static Logger logger = LoggerFactory.getLogger(BcryptClient.class);

    public static void main(String[] args) throws IOException {

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        logger.info("Enter the word to Bcrypt :");
        BufferedReader buf = new BufferedReader(new InputStreamReader(System.in));

        String word = buf.readLine();

        String bcryptWord = encoder.encode(word);

        logger.info("Encrypt Word : " + bcryptWord);

    }
}
