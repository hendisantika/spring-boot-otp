-- Dumping structure for table test.book
CREATE TABLE IF NOT EXISTS book (
  id     BIGINT(20)   NOT NULL AUTO_INCREMENT,
  author VARCHAR(255) NOT NULL,
  title  VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK_g0286ag1dlt4473st1ugemd0m (title)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table test.book: ~0 rows (approximately)
CREATE TABLE IF NOT EXISTS user (
  username  VARCHAR(255) NOT NULL,
  country   VARCHAR(255) DEFAULT NULL,
  full_name VARCHAR(255) DEFAULT NULL,
  password  VARCHAR(255) DEFAULT NULL,
  role      VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (username)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO user (username, country, full_name, password, role)
VALUES
	('admin', 'Indonesia', 'Admin-Hendi', '$2a$10$Z5F2Elzpnwx6kp0CYLmdo.Tcv8SZWMANvlr/PWr6.IxWWXnAi7KNC', 'ROLE_ADMIN'),
	('user', 'Jepang', 'User-Naruto', '$2a$10$KFsPE4H9buyijUht5nQU..qdpkDRA5q6zPeACtLVWAaDh3kMzPxXG', 'ROLE_USER');

	/*
	password and user name
	admin/admin
	user/user
	*/
